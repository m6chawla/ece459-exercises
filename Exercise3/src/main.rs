use std::thread;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    // let mut children = vec![];

    let handle = thread::spawn(|| {
        for i in 0..N {
            println!("hi from spawned thread {} ",i);

        }
    });

    handle.join().unwrap();
}
